//
//  main.cpp
//  Test
//
//  Created by Daniel Montero Cervantes on 08/03/14.
//  Copyright (c) 2014 Daniel Montero Cervantes. All rights reserved.
//

#include <iostream>
#include "PokerLib.h"

int main(int argc, const char * argv[])
{
    
    pokerlib::Hand my_hand;
    my_hand.evaluateHand();
    GetDeck()->init();
    int bestGame=0;
    for(int count=0;count<10000000;count++)
    {
        GetDeck()->init();
        while (GetDeck()->hasNextCard()){
            my_hand.add(GetDeck()->getNextCard());
            if(my_hand.isComplete())
            {
                my_hand.evaluateHand();
                if(my_hand.getNumberValue()>0x500000&&my_hand.getNumberValue()<0x600000)
                {
                    std::cout<<"Hand "<<my_hand.toString().c_str()<<std::endl;
                    std::cout<<"NUMBER VALUE   "<<std::hex<<my_hand.getNumberValue()<<std::endl;
                    if(my_hand.getNumberValue()>bestGame)
                        bestGame = my_hand.getNumberValue();
                }
                my_hand.clear();
            }
            
        }
        
    }
    std::cout<<"BEST HAND "<<std::hex<<bestGame<<std::endl;
    return 0;
}

