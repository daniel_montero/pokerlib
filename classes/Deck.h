//
//  Deck.h
//
//
//  Created by Daniel Montero Cervantes on 12/03/14.
//  Copyright (c) 2014 Daniel Montero Cervantes. All rights reserved.
//

#ifndef __PokerLib__Deck__
#define __PokerLib__Deck__

#include <iostream>
#include "Card.h"
#define GetDeck() pokerlib::Deck::getInstance()
class Card;
namespace pokerlib
{
class Deck{
public:
  static  Deck *getInstance();
    void init();
    Card getNextCard();
    bool hasNextCard();
private:
    Deck();
    ~Deck();
    std::vector<Card> m_cards;

};
}
#endif /* defined(__PokerLib__Deck__) */
