/*
 * Hand.cpp
 *
 *  Created on: 05/03/2014
 *      Author: danielmontero
 */

#include "Hand.h"
#include <algorithm>
#include <functional>
namespace pokerlib {
    
    Hand::Hand() {
        // TODO Auto-generated constructor stub
        m_intVal = 0;
        
    }
   
    
    Hand::~Hand() {
        // TODO Auto-generated destructor stub
    }
    bool Hand::isComplete()
    {
        return  m_hand.size()==5;
    }
    void Hand::add(pokerlib::Card c)
    {
        if( std::find(m_hand.begin(), m_hand.end(), c) == m_hand.end()) /* doesn't exist on vector already*/
        {
            m_hand.push_back(c);
        }
    }
	
    bool Hand::exist(pokerlib::Card c)
    {
        for(unsigned int i=0;i<m_hand.size();i++)
        {
            if(m_hand[i]==c)
                return true;
        }
        return false;
    }
	
    std::string Hand::toString()
    {
        std::string output="";
        for(unsigned int i=0;i<m_hand.size();i++)
        {
            output += " "+ getStringFromValue( m_hand[i].getValue()) + getStringFromSuit(m_hand[i].getSuit()) ;
        }
        
        return  output;
    }
    void Hand::evaluateHand()
    {
        m_counterMapSuit.clear();
        m_counterMapValue.clear();
        std::sort(m_hand.begin(), m_hand.end(),std::greater<Card>());
        for(unsigned int i=0;i<m_hand.size();i++)
        {
            LOGI("VALUE %d",m_hand[i].getValue());
            m_counterMapSuit.add(m_hand[i].getSuit());
            m_counterMapValue.add(m_hand[i].getValue());
        }
        LOGI("counterValue: %d",m_counterMapValue.getMax());
        LOGI("counterSuit: %d",m_counterMapSuit.getMax());
        switch(m_counterMapValue.getMax())
        {
            case 1:
            {
                
                //can either be STRAIGHT, FLUSH, STRAIGHT FLUSH, ROYAL FLUSH or HIGHEST_CARD
                //
                if(m_counterMapSuit.getMax()==5)
                {
                        //FLUSH, STRAIGHT FLUSH, ROYAL FLUSH
                    if(m_hand[0].getValue()-m_hand[4].getValue()==4)
                    {
                        if(m_hand[0].getValue()==ACE)
                        {
                            LOGI("ROYALFLUSH");
                            m_intVal = ROYAL_FLUSH;
                        
                        }
                        else
                        {
                            LOGI("STRAIGHT_FLUSH");
                            m_intVal = STRAIGHT_FLUSH;
                            
                        }
                        
                    }
                    else if (m_hand[0].getValue()==ACE && m_hand[1].getValue()==Value::FIVE)
                    {
                        LOGI("STRAIGHT_FLUSH");
                        m_intVal = STRAIGHT_FLUSH;
                        addToNumberValue(1, m_hand[1].getValue());
                        addToNumberValue(2, m_hand[2].getValue());
                        addToNumberValue(3, m_hand[3].getValue());
                        addToNumberValue(4, m_hand[4].getValue());
                        addToNumberValue(5, m_hand[0].getValue());
                        break;
                    }
                    else
                    {
                        LOGI("FLUSH");
                        m_intVal = FLUSH;
                    }
                    
                }
                else{
                        //STRAIGHT or HIGHEST_CARD
                    if(m_hand[0].getValue()-m_hand[4].getValue()==4)
                    {
                        LOGI("STRAIGHT");
                        m_intVal = STRAIGHT;
                    }
                    else if(m_hand[0].getValue()==ACE && m_hand[1].getValue()==FIVE)
                    {
                        LOGI("STRAIGHT");
                        m_intVal = STRAIGHT;
                        addToNumberValue(1, m_hand[1].getValue());
                        addToNumberValue(2, m_hand[2].getValue());
                        addToNumberValue(3, m_hand[3].getValue());
                        addToNumberValue(4, m_hand[4].getValue());
                        addToNumberValue(5, m_hand[0].getValue());
                        break;
                        
                    }
                    else
                    {
                        LOGI("HIGHEST CARD");
                        m_intVal = HIGHEST_CARD;
                    }
                }
                
                addToNumberValue(1, m_hand[0].getValue());
                addToNumberValue(2, m_hand[1].getValue());
                addToNumberValue(3, m_hand[2].getValue());
                addToNumberValue(4, m_hand[3].getValue());
                addToNumberValue(5, m_hand[4].getValue());
                
                
            }
            break;
            case 2:
            {
                //can either be PAIR or TWO_PAIR
                if(m_counterMapValue.getNumberOfElementsWith(2)==2)
                {
                    LOGI("Two Pairs");
                    m_intVal = TWO_PAIRS;
                    std::vector<Value> pairs = m_counterMapValue.getElementsWith(2);
                    
                    addToNumberValue(1, pairs[1]);
                    addToNumberValue(2, pairs[1]);
                    addToNumberValue(3, pairs[0]);
                    addToNumberValue(4, pairs[0]);
                    addToNumberValue(5, m_counterMapValue.getFirstElementWith(1));
                    
                }
                else
                {
                    LOGI("PAIR");
                    Value pair = m_counterMapValue.getFirstElementWith(2);
                    std::vector<Value> rest = m_counterMapValue.getElementsWith(1);
                    m_intVal = PAIR;
                    addToNumberValue(1, pair);
                    addToNumberValue(2, pair);
                    addToNumberValue(3, rest[2]);
                    addToNumberValue(4, rest[1]);
                    addToNumberValue(5, rest[0]);
                    
                }
                
            }
            break;
            case 3:
            {
                // can either be THREE_OF_A_KIND or FULL_HOUSE
                if(m_counterMapValue.getNumberOfElementsWith(2)==0)//how many pairs is in?
                {
                    std::vector<Value> vrest = m_counterMapValue.getElementsWith(1);
                    Value v_three = m_counterMapValue.getFirstElementWith(3);
                    LOGI("Three of a kind of %d's",v_three);
                    m_intVal = THREE_OF_A_KIND;
                    addToNumberValue(1, v_three);
                    addToNumberValue(2, v_three);
                    addToNumberValue(3, v_three);
                    addToNumberValue(4, vrest[1]);
                    addToNumberValue(5, vrest[0]);
                 
                }
                else
                {
                    Value v3 = m_counterMapValue.getFirstElementWith(3);
                    Value v2 = m_counterMapValue.getFirstElementWith(2);
                    LOGI("Full House");
                    m_intVal = FULL_HOUSE;
                    addToNumberValue(1, v3);
                    addToNumberValue(2, v3);
                    addToNumberValue(3, v3);
                    addToNumberValue(4, v2);
                    addToNumberValue(5, v2);
                    
                    
                }
                
            }
            break;
            case 4:
            {
                //is a FOUR_OF_A_KIND
                Value v = m_counterMapValue.getFirstElementWith(4);
                LOGI("Four of a kind of %d's",v);
                m_intVal = FOUR_OF_A_KIND;
                
                for(int i=1;i<5;i++)
                {
                    addToNumberValue(i, v);
                }
                addToNumberValue(5, m_counterMapValue.getFirstElementWith(1));
                    
                
            }
            break;
            default:
            {
                //WTF
            }
        
        }
        
    }
    void Hand::addToNumberValue(int pos, int value)
    {
        pos = transformToPosition(pos);
        m_intVal += value*pos;
    }
    int Hand::transformToPosition(int pos)
    {
        switch (pos) {
            case 1:
                {
                    return 0x010000;
                }
                break;
            case 2:
                {
                    return 0x001000;
                }
                break;
            case 3:
                {
                    return 0x000100;
                }
                break;
            case 4:
                {
                    return 0x000010;
                }
                break;
            case 5:
                {
                    return 0x000001;
                }
            
        }
        return 0;
    }
	void Hand::clear()
    {
        m_hand.clear();
        m_counterMapSuit.clear();
        m_counterMapValue.clear();
        m_intVal=0;
        
    }
    
    //COMPARATORS
    
    bool Hand::operator ==(const Hand &other) {
        
        return m_intVal == other.m_intVal;
        
    }
    
    bool Hand::operator <=(const Hand &other) {
        
        return m_intVal <= other.m_intVal;
    }
    
    bool Hand::operator >=(const Hand &other) {
        
        return m_intVal >= other.m_intVal;
    }
    
    bool Hand::operator <(const Hand &other) {
        
        return m_intVal < other.m_intVal;
    }
    
    bool Hand::operator >(const Hand &other) {
        
        return m_intVal > other.m_intVal;
    }
    
    
} /* namespace pokerlib */
