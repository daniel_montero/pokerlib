//
//  Definitions.h
//  Test
//
//  Created by Daniel Montero Cervantes on 08/03/14.
//  Copyright (c) 2014 Daniel Montero Cervantes. All rights reserved.
//

#ifndef Test_Definitions_h
#define Test_Definitions_h
namespace pokerlib
{
    enum Value{
        _VALUE_BEGIN=1,
		TWO=2,
		THREE,
		FOUR,
		FIVE,
		SIX,
		SEVEN,
		EIGHT,
		NINE,
		TEN,
		JOKER,
		QUEEN,
		KING,
		ACE,
        _VALUE_END,
	};
	enum Suit
	{
        _SUIT_BEGIN,
		HEARTS, // corazones
		DIAMONDS, // diamantes
		CLUBS,  //treboles
		SPADES, //picas
        _SUIT_END,
	};
    


}
#endif
