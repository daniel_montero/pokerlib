/*
 * Utils.h
 *
 *  Created on: 05/03/2014
 *      Author: danielmontero
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <string>
#include "Card.h"
#include "Definitions.h"

namespace pokerlib {
    
    
    
    //Card card(std::string str);
    Value getValueFromString(std::string str);
    std::string getStringFromValue(Value v);
    std::string getStringFromSuit(Suit s);
    Suit  getSuitFromString(std::string str);
    void  LOGI(const char *format, ... );
    

    
    
    
} /* namespace pokerlib */

#endif /* UTILS_H_ */
