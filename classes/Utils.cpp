/*
 * Utils.cpp
 *
 *  Created on: 05/03/2014
 *      Author: danielmontero
 */

#include "Utils.h"
#include <stdio.h>
#include <algorithm>
#define LOGS 0
namespace pokerlib {


/**
 Examples
 2 of hearts
 Q of diamonds
 * */
    /*
Card card(std::string str)
{
	size_t index = str.find(" ");
    
    if(index==std::string::npos)
        return Card(getValueFromString(""),getSuitFromString("")); // Returning default values

    Value v;
	Suit s;
	
    v = getValueFromString(str.substr(0, index-1));

    s = getSuitFromString(str.substr(index));

	return Card(v,s);
}
*/

void LOGI(const char *format, ... )
{
#if LOGS
    va_list args;
    va_start(args,format);
    std::string format_s = std::string(format) + std::string("\n");
    vprintf(format_s.c_str(),args);
    va_end(args);
#endif
}
Suit getSuitFromString(std::string str)
{
    std::transform(str.begin(), str.end(),str.begin(), ::toupper);
    if(str=="HEARTS")
        return pokerlib::HEARTS;
    if(str=="DIAMONDS")
        return pokerlib::DIAMONDS;
    if(str=="SPADES")
        return pokerlib::SPADES;
    
    return pokerlib::CLUBS;//DEFAULT VALUE

}
    
std::string getStringFromValue(Value v)
{
    switch (v) {
        case TWO:
            return "2";
        case THREE:
            return  "3";
        case FOUR:
            return  "4";
        case FIVE:
            return "5";
        case SIX:
            return "6";
        case  SEVEN:
            return "7";
        case EIGHT:
            return "8";
        case NINE:
            return "9";
        case TEN:
            return "10";
        case JOKER:
            return "J";
        case QUEEN:
            return "Q";
        case KING:
            return "K";
        case ACE:
            return "A";
        default:
            return "";
     }
    return "";
}
std::string getStringFromSuit(Suit s)
{
    switch (s) {
        case HEARTS:
            return "h";
            break;
        case CLUBS:
            return "c";
        case DIAMONDS:
            return "d";
        case SPADES:
            return "s";
        default:
            return "";
    }
    return "";
}
    
    
Value getValueFromString(std::string str)
{
    std::transform(str.begin(), str.end(),str.begin(), ::toupper);
    if(str == "TWO")
        return pokerlib::TWO;
    if(str == "THREE")
        return pokerlib::THREE;
    if(str == "FOUR")
        return pokerlib::FOUR;
    if(str == "FIVE")
        return pokerlib::FIVE;
    if(str == "SIX")
        return pokerlib::SIX;
    if(str == "SEVEN")
        return pokerlib::SEVEN;
    if(str == "EIGHT")
        return pokerlib::EIGHT;
    if(str == "NINE")
        return pokerlib::NINE;
    if(str == "TEN")
        return pokerlib::TEN;
    if(str == "JOKER")
        return pokerlib::JOKER;
    if(str == "QUEEN")
        return pokerlib::QUEEN;
    if(str == "KING")
        return pokerlib::KING;
    
    return pokerlib::ACE; // DEFAULT VALUE
    

}

} /* namespace pokerlib */
