//
//  CounterMap.h
//  PokerLib
//
//  Created by Daniel Montero Cervantes on 07/03/14.
//  Copyright (c) 2014 Daniel Montero Cervantes. All rights reserved.
//

#ifndef __PokerLib__CounterMap_
#define __PokerLib__CounterMap_

#include <iostream>
#include <map>
namespace  pokerlib
{

template<typename A_Type> class CounterMap
{
private:
    int m_max;
    std::map<A_Type, int> m_map;
public:
    CounterMap()
    {
        m_max=0;
    }
    
    void clear()
    {
        m_map.clear();
        m_max=0;
    }
    
    void add(A_Type a)
    {
        if(m_map.count(a))
        {
            int cnt = m_map[a];
            m_map[a]= ++cnt;
        }
        else
        {
            m_map[a]=1;
        }
        
        if(m_map[a]>m_max)
            m_max=m_map[a];
        
    }
    int getNumberOfElementsWith(int n)
    {
        int count=0;
        typename std::map<A_Type,int>::iterator it;
        for (it = m_map.begin(); it!=m_map.end(); ++it)
        {
            if((*it).second == n)
            {
                count++;
            }
        }
        return count;
    }
    A_Type getFirstElementWith(int n)
    {
        typename std::map<A_Type,int>::iterator it;
        for (it = m_map.begin(); it!=m_map.end(); ++it)
        {
            if((*it).second == n)
            {
                return((*it).first);
            }
        }
        return  A_Type();
    
    }
    std::vector<A_Type> getElementsWith(int n)
    {
        std::vector<A_Type> e_vector;
        
        typename std::map<A_Type,int>::iterator it;
        for (it = m_map.begin(); it!=m_map.end(); ++it)
        {
            if((*it).second == n)
            {
                e_vector.push_back((*it).first);
            }
        }
        return e_vector;
    }
    
    int getMax()
    {
        return  m_max;
    }
    
    int getCount(A_Type a)
    {
        if(m_map.count(a))
            m_map[a];
        return 0;

    }
    
};
}
#endif /* defined(__PokerLib__CounterMap__) */
