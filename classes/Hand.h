/*
 * Hand.h
 *
 *  Created on: 05/03/2014
 *      Author: danielmontero
 */

#ifndef HAND_H_
#define HAND_H_
#include "Definitions.h"
#include <vector>
#include "Card.h"
#include "CounterMap.h"

namespace pokerlib {

class Card;

enum HandType
{
    HIGHEST_CARD    = 0x100000,
    PAIR            = 0x200000,
    TWO_PAIRS       = 0x300000,
    THREE_OF_A_KIND = 0x400000,
    STRAIGHT        = 0x500000,
    FLUSH           = 0x600000,
    FULL_HOUSE      = 0x700000,
    FOUR_OF_A_KIND  = 0x800000,
    STRAIGHT_FLUSH  = 0x900000,
    ROYAL_FLUSH     = 0xA00000,
    // if you think about it royal flush
    // it's a simple straight flush with a
    // fancy name but for this game purposes
    // we will threated as a different hand
};
    
class Hand {
public:
	Hand();
	virtual ~Hand();
	void add(pokerlib::Card c);
	void evaluateHand();
    bool isComplete();
	void clear();
    bool exist(pokerlib::Card c);
    int  getNumberValue(){ return m_intVal; }
    std::string toString();
	bool operator == (const Hand &other);
	bool operator <= (const Hand &other);
	bool operator >= (const Hand &other);
	bool operator < (const Hand &other);
	bool operator > (const Hand &other);

private:
	unsigned int m_intVal;
    HandType m_handType;
    std::vector<pokerlib::Card> m_hand;
    CounterMap<Value> m_counterMapValue;
    CounterMap<Suit> m_counterMapSuit;
    void addToNumberValue(int pos, int value);
    int transformToPosition(int pos);
};

} /* namespace pokerlib */

#endif /* HAND_H_ */
