//
//  Deck.cpp
//  Test
//
//  Created by Daniel Montero Cervantes on 12/03/14.
//  Copyright (c) 2014 Daniel Montero Cervantes. All rights reserved.
//

#include "Deck.h"
#include <time.h>       /* time */
namespace pokerlib
{
    Deck::Deck()
    {
        m_cards.clear();
        srand ((unsigned int)time(NULL));
        
    }
    Deck::~Deck()
    {
        m_cards.clear();
    }

    Deck *Deck::getInstance()
    {
        static Deck *_this = new Deck();
        return _this;
    }
    void Deck::init()
    {
        m_cards.clear();
        //fill the deck
        for(int suit = _SUIT_BEGIN + 1; suit<_SUIT_END; suit++)//Suits
        {
            for( int value = _VALUE_BEGIN + 1;value<_VALUE_END;value++) // values
            {
                m_cards.push_back(Card((Value)value,(Suit)suit));
            }
        }
    }
    Card Deck::getNextCard()
    {
        
        int index = rand() % m_cards.size();
        
        Card card = m_cards[index];
        m_cards.erase(m_cards.begin()+index);
        return card;
    }
    bool Deck::hasNextCard()
    {
        return !m_cards.empty();
    }
}
