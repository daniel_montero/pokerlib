/*
 * Card.cpp
 *
 *  Created on: 04/03/2014
 *      Author: danielmontero
 */

#include "Card.h"
#include "PokerLib.h"

namespace pokerlib {
    
    Card::Card(Value v, Suit s){
        setSuit(s);
        setValue(v);
        
    }
    
    Card::~Card(){
        
    }
	/*Setters and Getters*/
    void Card::setValue(Value v)
    {
        m_value = v;
    }
    
    Value Card::getValue()
    {
        return m_value;
    }
    
    void Card::setSuit(Suit s)
    {
        m_suit=s;
    }
    
    Suit Card::getSuit()
    {
        return m_suit;
    }
    
    bool Card::operator ==(const Card &other) {
        
        return m_suit == other.m_suit && m_value == other.m_value;
    }
    bool Card::operator!=(const Card &other)
    {
        return !(m_suit == other.m_suit && m_value == other.m_value);
    }
    bool Card::operator<(const Card &other) const
    {
        return ((int)m_value)<((int)other.m_value);
    }
    bool Card::operator>(const Card &other) const
    {
        return  m_value>other.m_value;
    }
    bool Card::operator<=(const Card &other) const
    {
        return (int)m_value<=(int)other.m_value;
    }
    bool Card::operator>=(const Card &other) const
    {
        return m_value>=other.m_value;
    }
    
} /* namespace pokerlib */
