/*
 * Card.h
 *
 *  Created on: 04/03/2014
 *      Author: danielmontero
 */

#ifndef CARD_H_
#define CARD_H_
#include"PokerLib.h"
#include"Definitions.h"
namespace pokerlib {

class Card {
public:
	Card(Value v, Suit s);
	virtual ~Card();
	/*Setters and Getters*/
	void setValue(Value v);
	Value getValue();
	void setSuit(Suit s);
	Suit getSuit();
    bool operator ==(const Card &other);
    bool operator !=(const Card &other);
    bool operator <(const Card &other) const;
    bool operator >(const Card &other) const;
    bool operator <=(const Card &other) const;
    bool operator >=(const Card &other) const;
    
    
private:
	Value m_value;
	Suit m_suit;
};

} /* namespace pokerlib */

#endif /* CARD_H_ */
